﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.AI;

public class CatAI : MonoBehaviour
{

  private float timer;
  private NavMeshAgent agent;
  private Vector3 target;

  public int newTarget;

  // Start is called before the first frame update
  void Start()
  {
    agent = GetComponent<NavMeshAgent>();
  }

  // Update is called once per frame
  void Update()
  {
    timer += Time.deltaTime;

    if (timer >= newTarget)
    {
      setNewTarget();

      timer = 0;
    }
  }

  private void setNewTarget()
  {
    var myx = transform.position.x;
    var myz = transform.position.z;

    var xPos = myx + UnityEngine.Random.Range(myx - 2, myx + 2);
    var zPos = myz + UnityEngine.Random.Range(myz - 2, myz + 2);

    target = new Vector3(xPos, transform.position.y, zPos);

    agent.SetDestination(target);
    Debug.Log("Setting new target" + target);
  }
}
